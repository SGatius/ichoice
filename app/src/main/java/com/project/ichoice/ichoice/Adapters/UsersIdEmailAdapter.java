package com.project.ichoice.ichoice.Adapters;


import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.project.ichoice.ichoice.Objects.UsersIdEmail;

import java.util.List;

public class UsersIdEmailAdapter extends ArrayAdapter{

    private Context context;
    private List<UsersIdEmail> usersIdEmails;

    public UsersIdEmailAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<UsersIdEmail> objects) {
        super(context, resource, objects);
        this.context = context;
        this.usersIdEmails = objects;
    }

    @NonNull
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View view = super.getView(position, convertView, parent);
        TextView text = (TextView) view.findViewById(android.R.id.text1);
        text.setText(usersIdEmails.get(position).getMail());
        return view;
    }

    @Nullable
    @Override
    public Object getItem(int position) {
        return usersIdEmails.get(position);
    }
}
