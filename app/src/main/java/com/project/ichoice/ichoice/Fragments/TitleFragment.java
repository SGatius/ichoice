package com.project.ichoice.ichoice.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.project.ichoice.ichoice.R;

public class TitleFragment extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";

    private EditText editText;

    public static TitleFragment newInstance() {
        TitleFragment fragment = new TitleFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_title_election, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        editText = (EditText) view.findViewById(R.id.titleFrgElection);
    }

    public String getTitle(){
        return editText.getText().toString();
    }

    public boolean isEmpty(){

        if (editText.getText().toString().isEmpty()){
            editText.setError("This field can't be empty");
            return true;
        }
        else return false;
    }
}
