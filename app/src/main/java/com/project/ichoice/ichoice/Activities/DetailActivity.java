package com.project.ichoice.ichoice.Activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.project.ichoice.ichoice.Objects.Election;
import com.project.ichoice.ichoice.Objects.ListOfElections;
import com.project.ichoice.ichoice.Objects.ListOfElectionsRealized;
import com.project.ichoice.ichoice.Objects.ProgressDialogAuth;
import com.project.ichoice.ichoice.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class DetailActivity extends AppCompatActivity {

    private static final int FINGERPRINT_REQUEST = 7193;
    private static final String TAG = "DetailActivity: ";
    private int pos, posOption;
    private Button vote, seeResults;
    private List<Election> list;
    private List<String> listRealized;
    private HashMap<String, Integer> countResults;
    private ProgressDialogAuth pd;
    private EditText inputPassword;
    DatabaseReference reference;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView title = (TextView)findViewById(R.id.titleElection);
        TextView description = (TextView)findViewById(R.id.descriptionElection);
        TextView date = (TextView)findViewById(R.id.dateElection);

        vote = (Button)findViewById(R.id.btnVote);
        seeResults = (Button)findViewById(R.id.btnSeeResults);

        String txtDate;

        pos = getIntent().getIntExtra("POSPRESS", 0);
        list = ListOfElections.getInstance().getList();
        listRealized = ListOfElectionsRealized.getInstance().getList();

        pd = new ProgressDialogAuth(this, "Loading details...");

        reference = FirebaseDatabase.getInstance()
                .getReference("Elections Results")
                .child(list.get(pos).getId());

        pd.showProgressDialog();

        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                countResults = new HashMap<>();
                ArrayAdapter adapter;
                Date cDate = new Date(), eDate = new Date();
                int checkDate;
                ListView listView = (ListView)findViewById(R.id.optionsE);
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                String currentDate = obtainDate();
                String dateElection = list.get(pos).getDate();

                if (dateElection.isEmpty()){
                    checkDate = 0;
                }else {
                    try {
                        cDate = sdf.parse(currentDate);
                        eDate = sdf.parse(dateElection);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    checkDate = eDate.compareTo(cDate);
                }

                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    countResults.put(snapshot.getKey(), snapshot.getValue(Integer.class));
                }
                //Aqui dintre comprovarem a més l'accés a vot en disponibilitat de data
                if (listRealized.contains(list.get(pos).getId()) || checkDate < 0){
                    seeResults.setVisibility(View.VISIBLE);
                    vote.setVisibility(View.GONE);
                    adapter = new ArrayAdapter(getBaseContext(), android.R.layout.simple_list_item_1, list.get(pos).getOptions());
                    listView.setAdapter(adapter);
                }else {
                    adapter = new ArrayAdapter(getBaseContext(), android.R.layout.simple_list_item_single_choice, list.get(pos).getOptions());
                    listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
                    listView.setAdapter(adapter);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            posOption = position;
                        }
                    });
                }
                pd.hideProgressDialog();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        title.setText(list.get(pos).getTitle());
        description.setText(list.get(pos).getDescription());
        if (list.get(pos).getDate().isEmpty()){
            txtDate = "No date limit";
        }else {
            txtDate = "Date limit: " + list.get(pos).getDate();
        }
        date.setText(txtDate);

        vote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String password = PreferenceManager.getDefaultSharedPreferences(v.getContext())
                        .getString("user_password","");
                Boolean fingerprint = PreferenceManager.getDefaultSharedPreferences(v.getContext())
                        .getBoolean("user_fingerprint",false);
                if (password.isEmpty() && !fingerprint){
                    Toast.makeText(DetailActivity.this,
                            "You need introduce a password at the settings or enable the use about fingerprint to vote",
                            Toast.LENGTH_SHORT).show();
                }else if (!password.isEmpty() && !fingerprint){
                    checkAndVoteWithPassword(v.getContext(), password);
                }else if (fingerprint){
                    voteWithFingerprint();
                }
            }
        });
        seeResults.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), ResultsActivity.class);
                intent.putExtra("ID", list.get(pos).getId());
                startActivity(intent);
            }
        });
    }

    private String obtainDate() {
        Calendar date = Calendar.getInstance();
        String cDate = Integer.toString(date.get(Calendar.DAY_OF_MONTH))
                + "/"
                + Integer.toString(date.get(Calendar.MONTH) + 1)
                + "/"
                + Integer.toString(date.get(Calendar.YEAR));
        return cDate;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_share, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_share) {
            Intent intent = new Intent(this, ShareActivity.class);
            intent.putExtra("POS", pos);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    private void voteWithFingerprint() {

        startActivityForResult(new Intent(this, FingerprintActivity.class), FINGERPRINT_REQUEST);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == FINGERPRINT_REQUEST){
            Log.d(TAG, "Fingerprint succesful");
            String key = list.get(pos).getOptions().get(posOption);
            int count = countResults.get(key);
            count += 1;
            addVote(key, count);
            Toast.makeText(DetailActivity.this, "You vote succesful " + key,
                    Toast.LENGTH_SHORT).show();
            finish();
        }else if (resultCode == 0){
            Toast.makeText(this, "The vote was cancelled", Toast.LENGTH_SHORT).show();
            Log.d(TAG, "Fingerprint cancelled");
        }
    }

    private void checkAndVoteWithPassword(Context context, final String password) {
        inputPassword = new EditText(context);
        inputPassword.setInputType(InputType.TYPE_CLASS_TEXT |
                InputType.TYPE_TEXT_VARIATION_PASSWORD);
        new AlertDialog.Builder(context)
                .setTitle("Checking password")
                .setIcon(R.drawable.ichoicelogo)
                .setMessage("Introduce your private password")
                .setView(inputPassword)
                .setPositiveButton("CHECK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (inputPassword.getText().toString().equals(password)){
                            String key = list.get(pos).getOptions().get(posOption);
                            int count = countResults.get(key);
                            count += 1;
                            addVote(key ,count);
                            Toast.makeText(DetailActivity.this, "You vote succesful " + key,
                                    Toast.LENGTH_SHORT).show();
                            finish();
                        }else {
                            Toast.makeText(DetailActivity.this,
                                    "The password introduced is not correct",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .show();
    }

    private void addVote(String key, int count) {
        FirebaseDatabase.getInstance()
                .getReference("Elections Results")
                .child(list.get(pos).getId())
                .child(key)
                .setValue(count);
        Log.d(TAG, "Vote added");
        FirebaseDatabase.getInstance()
                .getReference("Users")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .child("Elections Realized")
                .child(list.get(pos).getId())
                .setValue("OK");
        Log.d(TAG, "Election added at the list of votes realized");
    }
}
