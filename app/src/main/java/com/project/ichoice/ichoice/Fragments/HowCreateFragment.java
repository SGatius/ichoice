package com.project.ichoice.ichoice.Fragments;

import android.app.Fragment;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.project.ichoice.ichoice.R;

/**
 * Created by santi on 28/07/17.
 */

public class HowCreateFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.how_create_view, container, false);
    }
}
