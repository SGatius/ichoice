package com.project.ichoice.ichoice.Objects;

import java.util.List;

/**
 * Created by santi on 11/06/17.
 */

public class Election {
    private String title, description, date, id;
    private List<String> options;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getDate() {
        return date;
    }

    public List<String> getOptions() {
        return options;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setOptions(List<String> options) {
        this.options = options;
    }
}
