package com.project.ichoice.ichoice.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.project.ichoice.ichoice.R;

import java.util.ArrayList;
import java.util.List;


public class OptionsFragment extends Fragment {

    private static final String TAG = "Options Fragment";
    private static int n;

    private List<String> options;
    private static List<EditText> listEditText;

    public static OptionsFragment newInstance() {
        OptionsFragment fragment = new OptionsFragment();
        listEditText = new ArrayList<>();
        n = 3;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        options = new ArrayList<>();
        listEditText = new ArrayList<>();
        return inflater.inflate(R.layout.fragment_options_election, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        EditText option1 = (EditText) view.findViewById(R.id.optionElection1);
        EditText option2 = (EditText) view.findViewById(R.id.optionElection2);
        listEditText.add(option1);
        listEditText.add(option2);
        FloatingActionButton fabAdd = (FloatingActionButton) view.findViewById(R.id.fabAdd);
        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addEditText(v.getRootView());
            }
        });
    }

    //Add a new EditText on the LinearLayout
    private void addEditText(View v) {
        LinearLayout linear = (LinearLayout)v.findViewById(R.id.optionsElection);
        EditText newEditText = new EditText(v.getContext());
        newEditText.setInputType(InputType.TYPE_TEXT_FLAG_AUTO_CORRECT);
        newEditText.setHint("Option " + String.valueOf(n));
        linear.addView(newEditText);
        Log.d(TAG, "Added EditText " + String.valueOf(n));
        listEditText.add(newEditText);
        Log.d(TAG, "Added a new EditText at the list of editText");
        n = n + 1;
        Log.d(TAG, "Increase the number of EditText");
    }

    public boolean checkOptions(){
        boolean checkOptions = true;
        for (int i = 0; i < listEditText.size(); i++){
            if (listEditText.get(i).getText().toString().isEmpty()){
                listEditText.get(i).setError("This field can't be empty");
                checkOptions = false;
                Log.e(TAG, "The field number "+ String.valueOf(i) + " is empty");
            }
        }
        return checkOptions;
    }

    public List getOptions(){
        for (int i = 0; i < listEditText.size(); i++){
            options.add(listEditText.get(i).getText().toString());
        }
        return options;
    }
}
