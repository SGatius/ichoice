package com.project.ichoice.ichoice.Activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.project.ichoice.ichoice.Objects.ProgressDialogAuth;
import com.project.ichoice.ichoice.Objects.UsersIdEmail;
import com.project.ichoice.ichoice.R;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{

    private static final String TAG = "LoginActivity";
    private AutoCompleteTextView email;
    private EditText password;
    private FirebaseAuth mAuth;
    private ProgressDialogAuth pD;
    private int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        email = (AutoCompleteTextView) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        findViewById(R.id.logUser).setOnClickListener(this);
        findViewById(R.id.createAccount).setOnClickListener(this);
        findViewById(R.id.pass_forget).setOnClickListener(this);

        mAuth = FirebaseAuth.getInstance();
        pD = new ProgressDialogAuth(this, getString(R.string.pd_auth));

    }


    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart method");
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null){
            addUserToDatabase(currentUser);
            startActivity(new Intent(this, MainActivity.class));
        }
    }

    private void addUserToDatabase(FirebaseUser currentUser) {
        UsersIdEmail idEmail = new UsersIdEmail(currentUser.getUid(), currentUser.getEmail());
        DatabaseReference reference = FirebaseDatabase.getInstance()
                .getReference();
        reference.child("users")
                .child(currentUser.getUid())
                .setValue(idEmail);
        reference.child("usersInfo")
                .child(currentUser.getUid())
                .setValue(idEmail);
        String instanceId = FirebaseInstanceId.getInstance().getToken();
        if (instanceId != null) {
            reference.child("users")
                    .child(currentUser.getUid())
                    .child("instanceId")
                    .setValue(instanceId);
            reference.child("usersInfo")
                    .child(currentUser.getUid())
                    .child("instanceId")
                    .setValue(instanceId);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.logUser:
                Log.d(TAG, "Button's login pressed");
                if (count >= 4){
                    findViewById(R.id.pass_forget).setVisibility(View.VISIBLE);
                }
                count += 1;
                Log.d(TAG, "One count more");
                loginUser();
                break;
            case R.id.createAccount:
                Log.d(TAG, "TextView's create account pressed");
                startActivity(new Intent(this, CreateAccountActivity.class));
                break;
            case R.id.pass_forget:
                sendMail();
                break;
        }
    }

    private void sendMail() {
        String mail = email.getText().toString();
        if (mail.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(mail).matches()){
            Log.e(TAG,"Error on email");
            email.setError(getString(R.string.msg_err_email));
        }else {
            Log.d(TAG, "Reset email sended");
            email.setError(null);
            mAuth.sendPasswordResetEmail(mail);
            count = 0;
            findViewById(R.id.pass_forget).setVisibility(View.INVISIBLE);
            Toast.makeText(this, R.string.msg_reset_password, Toast.LENGTH_LONG).show();
        }
    }

    private void loginUser() {
        if (validate()){
            Log.d(TAG, "Login: " + email.getText().toString());
            pD.showProgressDialog();
            mAuth.signInWithEmailAndPassword(email.getText().toString(), password.getText().toString())
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            Log.d(TAG, "signInWithEmail:onComplete:" + task.isSuccessful());
                            pD.hideProgressDialog();
                            if (!task.isSuccessful()) {
                                Log.w(TAG, "signInWithEmail:failed", task.getException());
                                Toast.makeText(LoginActivity.this, task.getException().getMessage(),
                                        Toast.LENGTH_SHORT).show();
                            }else {
                                addUserToDatabase(mAuth.getCurrentUser());
                                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                            }

                        }
                    });
        }
    }

    private boolean validate() {

        String mail = email.getText().toString();
        String pass = password.getText().toString();
        if (mail.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(mail).matches()){
            Log.e(TAG,"Error on email");
            email.setError(getString(R.string.msg_err_email));
            return false;
        }else {
            email.setError(null);
        }
        if (pass.isEmpty() || pass.length() < 5){
            Log.e(TAG, "Error on password");
            password.setError(getString(R.string.msg_err_pass));
            return false;
        }else {
            password.setError(null);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }
}