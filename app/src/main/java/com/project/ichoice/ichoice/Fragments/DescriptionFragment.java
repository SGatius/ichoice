package com.project.ichoice.ichoice.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.project.ichoice.ichoice.R;

public class DescriptionFragment extends Fragment {

    private EditText description;

    public static DescriptionFragment newInstance() {
        DescriptionFragment fragment = new DescriptionFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_description_election, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        description = (EditText) view.findViewById(R.id.descriptionFrgElection);
    }

    public String getDescription(){
        return description.getText().toString();
    }

    public boolean isEmpty(){

        if (description.getText().toString().isEmpty()){
            description.setError("This field can't be empty");
            return true;
        }
        else return false;
    }
}
