package com.project.ichoice.ichoice.Objects;

import java.util.ArrayList;
import java.util.List;


public class ListOfElectionsRealized {

    private static ListOfElectionsRealized listOfElectionsRealized;
    private List<String> list;

    private ListOfElectionsRealized(){
        list = new ArrayList<>();
    }

    public static ListOfElectionsRealized getInstance(){
        if (listOfElectionsRealized == null){
            listOfElectionsRealized = new ListOfElectionsRealized();
        }
        return listOfElectionsRealized;
    }

    public void add(String id){
        list.add(id);
    }

    public List getList(){
        return list;
    }
}