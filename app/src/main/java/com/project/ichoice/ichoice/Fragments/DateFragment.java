package com.project.ichoice.ichoice.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.Toast;

import com.project.ichoice.ichoice.R;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFragment extends Fragment {

    private CalendarView calendarView;
    private boolean assign;

    public static DateFragment newInstance() {
        DateFragment fragment = new DateFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        assign = false;
        return inflater.inflate(R.layout.fragment_date_election, container, false);
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        calendarView = (CalendarView) view.findViewById(R.id.calendarView);
        calendarView.setOnDateChangeListener(dateListener);
        view.findViewById(R.id.btnAssignDate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                assign = true;
                Log.d("Fecha: ", getDate());
                Toast.makeText(view.getContext(), "Date assigned", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public String getDate(){
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(new Date(calendarView.getDate()));
    }

    public boolean isAssign(){
        return assign;
    }

    CalendarView.OnDateChangeListener dateListener = new CalendarView.OnDateChangeListener() {
        @Override
        public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
            String selectedDate = dayOfMonth + "/" + (month + 1) + "/" + year;
            try {
                calendarView.setDate(new SimpleDateFormat("dd/MM/yyyy").parse(selectedDate).getTime(), true, true);
            }catch (java.text.ParseException e){
                e.printStackTrace();
            }
        }
    };
}
