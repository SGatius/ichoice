package com.project.ichoice.ichoice.Activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.project.ichoice.ichoice.Objects.ProgressDialogAuth;
import com.project.ichoice.ichoice.R;

public class CreateAccountActivity extends AppCompatActivity implements View.OnClickListener{

    private static final String TAG = "CreateAccountActivity";

    private AutoCompleteTextView email;
    private EditText password;
    private FirebaseAuth mAuth;
    private FirebaseDatabase database;
    private DatabaseReference myRef;
    ProgressDialogAuth pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("Users");

        email = (AutoCompleteTextView) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        findViewById(R.id.createUser).setOnClickListener(this);
        findViewById(R.id.loginAccount).setOnClickListener(this);

        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.createUser:
                createUser();
                break;
            case R.id.loginAccount:
                finish();
                break;
        }
    }

    private void createUser() {
        if (validate()){
            Log.d(TAG, "Create account: " + email.getText().toString());
            pd = new ProgressDialogAuth(this, getString(R.string.pd_crt_acct));
            pd.showProgressDialog();
            mAuth.createUserWithEmailAndPassword(email.getText().toString(), password.getText().toString())
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            Log.d(TAG, "createUserWithEmail:onComplete:" + task.isSuccessful());

                            pd.hideProgressDialog();
                            if (!task.isSuccessful()) {
                                Toast.makeText(CreateAccountActivity.this, task.getException().getMessage(),
                                        Toast.LENGTH_SHORT).show();
                            }else {
                                String id = mAuth.getCurrentUser().getUid();
                                myRef.child(id)
                                        .child("UserEmail")
                                        .setValue(email.getText().toString());
                                myRef.child(id)
                                        .child("Password")
                                        .setValue(password.getText().toString());
                                loginUserAndSendVerification();
                            }

                        }
                    });
        }
    }

    private boolean validate() {

        String mail = email.getText().toString();
        String pass = password.getText().toString();
        if (mail.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(mail).matches()){
            Log.e(TAG,"Error on email");
            email.setError(getString(R.string.msg_err_email));
            return false;
        }else {
            email.setError(null);
        }
        if (pass.isEmpty() || pass.length() < 5){
            Log.e(TAG, "Error on password");
            password.setError(getString(R.string.msg_err_pass));
            return false;
        }else {
            password.setError(null);
        }
        return true;
    }
    private void loginUserAndSendVerification() {

        Log.d(TAG, "Login: " + email.getText().toString());
        pd.showProgressDialog();
        pd.setMessage(getString(R.string.pd_auth));
        mAuth.signInWithEmailAndPassword(email.getText().toString(), password.getText().toString())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {Log.d(TAG, "signInWithEmail:onComplete:" + task.isSuccessful());
                            pd.hideProgressDialog();
                            if (!task.isSuccessful()) {
                                Log.w(TAG, "signInWithEmail:failed", task.getException());
                                Toast.makeText(CreateAccountActivity.this, task.getException().getMessage(),
                                        Toast.LENGTH_SHORT).show();
                            }else {
                                Log.d(TAG, "Sign in and process to send email verification");
                                final FirebaseUser user = mAuth.getCurrentUser();
                                user.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            Log.d(TAG, "Verification email sent to " + user.getEmail());
                                            Toast.makeText(CreateAccountActivity.this,
                                                    "Verification email sent to " + user.getEmail(),
                                                    Toast.LENGTH_LONG).show();
                                            startActivity(new Intent(getApplicationContext(), MainActivity.class));
                                        } else {
                                            Log.e(TAG, "sendEmailVerification", task.getException());
                                            Toast.makeText(CreateAccountActivity.this,
                                                    task.getException().getMessage(),
                                                    Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                            }
                        }
                    });

    }
}