package com.project.ichoice.ichoice.Objects;

import java.util.ArrayList;
import java.util.List;



public class ListOfElections {

    private static ListOfElections listOfElections;
    private List<Election> list;

    private ListOfElections(){
        list = new ArrayList<>();
    }

    public static ListOfElections getInstance(){
        if (listOfElections == null){
            listOfElections = new ListOfElections();
        }
        return listOfElections;
    }

    public void add(Election election){
        list.add(election);
    }

    public List getList(){
        return list;
    }

    public void remove(Election election) {
        list.remove(election);
    }

    public void removeAll(){
        if (!list.isEmpty()){
            list.clear();
        }
    }
}
