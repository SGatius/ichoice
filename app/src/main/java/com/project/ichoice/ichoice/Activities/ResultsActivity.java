package com.project.ichoice.ichoice.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.project.ichoice.ichoice.Adapters.ResultsAdapter;
import com.project.ichoice.ichoice.Objects.ListResults;
import com.project.ichoice.ichoice.Objects.ProgressDialogAuth;
import com.project.ichoice.ichoice.R;

import java.util.ArrayList;
import java.util.List;

public class ResultsActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ProgressDialogAuth pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);

        String id;
        Query data;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        recyclerView = (RecyclerView)findViewById(R.id.resultsElection);
        id = getIntent().getStringExtra("ID");

        pd = new ProgressDialogAuth(this, "Loading...");
        pd.showProgressDialog();

        data = FirebaseDatabase.getInstance().getReference("Elections Results").child(id);

        data.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<ListResults> results = new ArrayList<>();
                ResultsAdapter resultsAdapter;
                for (DataSnapshot data : dataSnapshot.getChildren()){
                    ListResults rslt = new ListResults(data.getKey(), data.getValue(Long.class));
                    results.add(rslt);
                }
                pd.hideProgressDialog();
                resultsAdapter = new ResultsAdapter(results);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(resultsAdapter);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
}
