package com.project.ichoice.ichoice.Objects;


public class UsersIdEmail {

    private String uID, mail, instanceID;

    public UsersIdEmail(String uID, String mail) {
        this.uID = uID;
        this.mail = mail;
    }

    public String getuID() {
        return uID;
    }

    public String getMail() {
        return mail;
    }

    public String getInstanceID() {
        return instanceID;
    }

    public void setInstanceID(String instanceID) {
        this.instanceID = instanceID;
    }
}
