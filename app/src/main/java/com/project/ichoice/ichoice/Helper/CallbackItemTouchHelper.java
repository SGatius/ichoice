package com.project.ichoice.ichoice.Helper;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.project.ichoice.ichoice.Objects.Election;
import com.project.ichoice.ichoice.Objects.ListOfElections;


public class CallbackItemTouchHelper extends ItemTouchHelper.Callback {

    private String userId;
    private RecyclerView mRecyclerView;
    private Context context;

    public CallbackItemTouchHelper(Context context) {
        this.context = context;
        userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
    }

    @Override
    public boolean isItemViewSwipeEnabled() {
        return true;
    }

    @Override
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
        int swipeFlags = ItemTouchHelper.END;
        mRecyclerView = recyclerView;
        return makeMovementFlags(dragFlags, swipeFlags);
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        return false;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        int position = viewHolder.getAdapterPosition();
        Election election = (Election)ListOfElections.getInstance().getList().get(position);
        ListOfElections.getInstance().remove(election);
        FirebaseDatabase
                .getInstance()
                .getReference("Users")
                .child(userId)
                .child("Elections")
                .child(election.getId())
                .removeValue();
        mRecyclerView.getAdapter().notifyDataSetChanged();
        mRecyclerView.getAdapter().notifyItemRemoved(position);
        Toast.makeText(context, "Removed from the list successfully", Toast.LENGTH_SHORT).show();
    }
}