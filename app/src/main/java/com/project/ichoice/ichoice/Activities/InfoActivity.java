package com.project.ichoice.ichoice.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.project.ichoice.ichoice.Fragments.HowCreateFragment;
import com.project.ichoice.ichoice.Fragments.HowToVoteFragment;
import com.project.ichoice.ichoice.Fragments.MainScreenFragment;
import com.project.ichoice.ichoice.Fragments.SettingsFragment;
import com.project.ichoice.ichoice.R;

public class InfoActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Spinner spinner = (Spinner)findViewById(R.id.spinner_info);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.category_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String item = parent.getItemAtPosition(position).toString();
        if (item.equals("How to create") || item.equals("Cómo crear")){
            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frg_info, new HowCreateFragment())
                    .commit();
        }else if (item.equals("Settings") || item.equals("Ajustes")){
            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frg_info, new SettingsFragment())
                    .commit();
        }else if (item.equals("Main screen") || item.equals("Pantalla principal")){
            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frg_info, new MainScreenFragment())
                    .commit();
        }else if (item.equals("How to vote") || item.equals("Cómo votar")){
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.frg_info, new HowToVoteFragment())
                .commit();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
