package com.project.ichoice.ichoice.Activities;

import android.animation.Animator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.project.ichoice.ichoice.Adapters.UsersIdEmailAdapter;
import com.project.ichoice.ichoice.Objects.Election;
import com.project.ichoice.ichoice.Objects.ListOfElections;
import com.project.ichoice.ichoice.Objects.ProgressDialogAuth;
import com.project.ichoice.ichoice.Objects.UsersIdEmail;
import com.project.ichoice.ichoice.R;

import java.util.ArrayList;
import java.util.List;

public class ShareActivity extends AppCompatActivity {

    private ListView listView;
    private UsersIdEmailAdapter adapter;
    private UsersIdEmail owner;
    private ProgressDialogAuth pd;
    private int pos;
    private List<String> listUsersChecked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("usersInfo");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        FloatingActionButton fabShare = (FloatingActionButton)findViewById(R.id.fabShare);
        listView = (ListView)findViewById(R.id.listUsers);

        owner = new UsersIdEmail(FirebaseAuth.getInstance().getCurrentUser().getUid(),
                FirebaseAuth.getInstance().getCurrentUser().getEmail());
        pos = getIntent().getIntExtra("POS", 0);
        listUsersChecked = new ArrayList<>();

        pd = new ProgressDialogAuth(this, "Loading users...");
        pd.showProgressDialog();

        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            List<UsersIdEmail> listIdMail = new ArrayList<>();
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    String id = snapshot.child("uID").getValue(String.class);
                    String mail = snapshot.child("mail").getValue(String.class);
                    UsersIdEmail im = new UsersIdEmail(id, mail);
                    im.setInstanceID(snapshot.child("instanceId").getValue(String.class));
                    if (!id.equals(owner.getuID())){
                        listIdMail.add(im);
                    }else {
                        owner.setInstanceID(snapshot.child("instanceId").getValue(String.class));
                    }
                }
                adapter = new UsersIdEmailAdapter(getBaseContext(), android.R.layout.simple_list_item_multiple_choice, listIdMail);
                listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
                listView.setAdapter(adapter);
                pd.hideProgressDialog();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        fabShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Interpolator interpolator = AnimationUtils.loadInterpolator(getBaseContext(),
                        android.R.interpolator.accelerate_cubic);
                v.animate().rotation(360f)
                        .setInterpolator(interpolator)
                        .setStartDelay(5)
                        .setListener(new Animator.AnimatorListener() {
                            boolean userExist;
                            @Override
                            public void onAnimationStart(Animator animation) {
                                userExist = checkUsersChecked();
                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                if (userExist){
                                    if (checkPreferencesAndConnection()){
                                        share();
                                    }
                                }
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        });
            }
        });
    }

    private boolean checkPreferencesAndConnection(){
        Boolean wifiConnected, mobileConnected;
        Boolean shareWifi = PreferenceManager
                .getDefaultSharedPreferences(getBaseContext())
                .getBoolean("user_share", false);

        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (shareWifi){
            if (networkInfo != null && networkInfo.isConnected()){
                wifiConnected = networkInfo.getType() == ConnectivityManager.TYPE_WIFI;
                if (wifiConnected){
                    return true;
                }else {
                    Snackbar.make(getCurrentFocus(), "Error occurred. No WiFi connection detected", Snackbar.LENGTH_LONG)
                            .setAction("Settings", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                                }
                            })
                            .setActionTextColor(Color.RED)
                            .show();
                    return false;
                }
            }else {
                Snackbar.make(getCurrentFocus(), "Error occurred. No network connection", Snackbar.LENGTH_LONG)
                        .setAction("Settings", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                            }
                        })
                        .setActionTextColor(Color.RED)
                        .show();
                return false;
            }

        }else{
            if (networkInfo != null && networkInfo.isConnected()){
                wifiConnected = networkInfo.getType() == ConnectivityManager.TYPE_WIFI;
                mobileConnected = networkInfo.getType() == ConnectivityManager.TYPE_MOBILE;
                if (wifiConnected || mobileConnected){
                    return true;
                }else {
                    Snackbar.make(getCurrentFocus(), "Error occurred. No network detected", Snackbar.LENGTH_LONG)
                            .setAction("Settings", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    startActivity(new Intent(Settings.ACTION_SETTINGS));
                                }
                            })
                            .setActionTextColor(Color.RED)
                            .show();
                    return false;
                }
            }else {
                Snackbar.make(getCurrentFocus(), "Error occurred. No network connection", Snackbar.LENGTH_LONG)
                        .setAction("Settings", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                startActivity(new Intent(Settings.ACTION_SETTINGS));
                            }
                        })
                        .setActionTextColor(Color.RED)
                        .show();
                return false;
            }
        }
    }

    private void share() {
        DatabaseReference database = FirebaseDatabase.getInstance().getReference();
        Election election = (Election)ListOfElections.getInstance().getList().get(pos);
        for (int i = 0; i < listUsersChecked.size(); i++){
            database.child("Users")
                    .child(listUsersChecked.get(i))
                    .child("Elections")
                    .child(election.getId())
                    .setValue(election);
            database.child("users")
                    .child(listUsersChecked.get(i))
                    .child("mail")
                    .setValue(owner.getMail());
        }
        Toast.makeText(this, "Share succesfully", Toast.LENGTH_SHORT).show();
        finish();
    }

    private boolean checkUsersChecked() {
        SparseBooleanArray checked = listView.getCheckedItemPositions();
        for (int i = 0; i < checked.size(); i++){
            int pos = checked.keyAt(i);
            if (checked.valueAt(i)){
                UsersIdEmail im = (UsersIdEmail)adapter.getItem(pos);
                listUsersChecked.add(im.getuID());
            }
        }
        if (listUsersChecked.isEmpty()){
            Toast.makeText(this, "If you want share to someone you must select an user.", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            return true;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_sharing, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.action_select_all:
                for (int i = 0; i < listView.getAdapter().getCount(); i++){
                    listView.setItemChecked(i, true);
                }
                break;
            case R.id.action_clear_all:
                for (int i = 0; i < listView.getAdapter().getCount(); i++){
                    listView.setItemChecked(i, false);
                }
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
