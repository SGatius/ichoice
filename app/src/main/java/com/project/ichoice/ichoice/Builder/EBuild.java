package com.project.ichoice.ichoice.Builder;

import java.util.List;

/**
 * Created by santi on 11/06/17.
 */

public class EBuild extends ElectionBuilder {

    @Override
    public void Title(String title) {
        election.setTitle(title);
    }

    @Override
    public void Description(String description) {
        election.setDescription(description);
    }

    @Override
    public void Options(List<String> options) {
        election.setOptions(options);
    }

    @Override
    public void Date(String date) {
        election.setDate(date);
    }
}
