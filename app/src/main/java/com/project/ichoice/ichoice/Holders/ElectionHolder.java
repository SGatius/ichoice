package com.project.ichoice.ichoice.Holders;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.project.ichoice.ichoice.Activities.DetailActivity;
import com.project.ichoice.ichoice.Objects.Election;
import com.project.ichoice.ichoice.Objects.ListOfElections;
import com.project.ichoice.ichoice.R;


public class ElectionHolder extends ViewHolder implements OnClickListener{
    private View mView;
    private Context context;

    public ElectionHolder(View itemView){
        super(itemView);
        mView = itemView;
        context = mView.getContext();
        itemView.setOnClickListener(this);
    }

    public void bindElection(Election election) {
        ListOfElections.getInstance().add(election);
        TextView field = (TextView) mView.findViewById(R.id.listTitle);
        TextView field2 = (TextView) mView.findViewById(R.id.listDescription);
        TextView field3 = (TextView) mView.findViewById(R.id.listDate);
        field.setText(election.getTitle());
        field2.setText(election.getDescription());
        if (election.getDate().isEmpty()){
            field3.setText("No date");
        }else {
            field3.setText(election.getDate());
        }
    }

    @Override
    public void onClick(View v) {
        int posPress = getLayoutPosition();
        Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtra("POSPRESS", posPress);
        context.startActivity(intent);
    }
}
