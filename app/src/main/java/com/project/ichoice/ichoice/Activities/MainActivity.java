package com.project.ichoice.ichoice.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.project.ichoice.ichoice.Helper.CallbackItemTouchHelper;
import com.project.ichoice.ichoice.Holders.ElectionHolder;
import com.project.ichoice.ichoice.Objects.Election;
import com.project.ichoice.ichoice.Objects.ListOfElections;
import com.project.ichoice.ichoice.Objects.ListOfElectionsRealized;
import com.project.ichoice.ichoice.Objects.ProgressDialogAuth;
import com.project.ichoice.ichoice.R;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "MainActivity: ";
    private FirebaseAuth mAuth;
    private FirebaseUser user;
    private SharedPreferences pref;
    private ProgressDialogAuth pd;
    private List<Election> electionList;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private FirebaseRecyclerAdapter adapter;
    private Query dataElection;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ListOfElections.getInstance().removeAll();

        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        pref = PreferenceManager.getDefaultSharedPreferences(this);

        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();

        final Query dataRealized = FirebaseDatabase.getInstance()
                .getReference("Users")
                .child(user.getUid())
                .child("Elections Realized");
        dataRealized.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    ListOfElectionsRealized.getInstance().add(snapshot.getKey());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        dataElection = FirebaseDatabase.getInstance()
                .getReference("Users")
                .child(user.getUid())
                .child("Elections")
                .orderByChild("title");

        recyclerView = (RecyclerView)findViewById(R.id.listElections);

        pd = new ProgressDialogAuth(this, "Loading...");
        pd.showProgressDialog();

        dataElection.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                electionList = new ArrayList<>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    electionList.add(snapshot.getValue(Election.class));
                }
                if (!user.isEmailVerified()){
                    recyclerView.setVisibility(View.GONE);
                    findViewById(R.id.mailVerify).setVisibility(View.VISIBLE);
                }else if (electionList.isEmpty()){
                    recyclerView.setVisibility(View.INVISIBLE);
                    findViewById(R.id.isListEmpty).setVisibility(View.VISIBLE);
                }
                pd.hideProgressDialog();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(view.getContext(), CreateElectionActivity.class));
                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View header = navigationView.getHeaderView(0);
        //Modify the TextView with the email of the user and the user name
        TextView userEmail = (TextView)header.findViewById(R.id.userEmail);
        TextView userName = (TextView)header.findViewById(R.id.userName);

        if (user != null){
            userEmail.setText(user.getEmail());
        }
        if (!pref.contains("display_name")){
            userName.setText("Unknown");
        }else{
            userName.setText(pref.getString("display_name","Unknown"));
        }

        if (user.isEmailVerified()) {
            ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new CallbackItemTouchHelper(this));
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));
            //recyclerView.setLayoutManager(new GridLayoutManager(getBaseContext(),1));
            //recyclerView.setLayoutManager(new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL));
            adapter = new FirebaseRecyclerAdapter<Election, ElectionHolder>(Election.class, R.layout.list_items, ElectionHolder.class, dataElection) {
                @Override
                protected void populateViewHolder(ElectionHolder electionHolder, Election model, int position) {
                    electionHolder.bindElection(model);
                }
            };
            recyclerView.setAdapter(adapter);
            swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.swipeRefresh);
            swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                    R.color.colorPrimaryDark, R.color.colorAccent, R.color.colorBackground);
            itemTouchHelper.attachToRecyclerView(recyclerView);
            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    dataElection.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (!electionList.isEmpty()){
                                electionList.clear();
                            }
                            for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                                electionList.add(snapshot.getValue(Election.class));
                            }
                            adapter.notifyDataSetChanged();
                            swipeRefreshLayout.setRefreshing(false);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
            });
        }
        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            new AlertDialog.Builder(this)
                    .setTitle("Exit app")
                    .setIcon(R.drawable.ichoicelogo)
                    .setMessage("Are you sure you want close session and exit the app?")
                    .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            FirebaseAuth.getInstance().signOut();
                            finishAffinity();
                        }
                    })
                    .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    })
                    .show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            if (pref.contains("user_password") && !pref.getString("user_password", "").isEmpty()){
                String password = pref.getString("user_password", "");
                confirmUserCanChangeSettings(password);
            }else {
                startActivity(new Intent(this, SettingsActivity.class));
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_create) {
            startActivity(new Intent(this, CreateElectionActivity.class));
        } else if (id == R.id.nav_manage) {
            if (pref.contains("user_password") && !pref.getString("user_password", "").isEmpty()){
                String password = pref.getString("user_password", "");
                confirmUserCanChangeSettings(password);
            }else {
                startActivity(new Intent(this, SettingsActivity.class));
            }

        } else if (id == R.id.nav_share) {
            //Here the code to invite
        } else if (id == R.id.nav_info) {
            startActivity(new Intent(this, InfoActivity.class));
        } else if (id == R.id.nav_change_password) {
            startActivity(new Intent(this, ChangePasswordActivity.class));
            //Create activity to change password
        } else if (id == R.id.nav_logout){
            FirebaseAuth.getInstance().signOut();
            Log.d(TAG, "Sign out");
            startActivity(new Intent(this, LoginActivity.class));
        } else if (id == R.id.nav_exit){
            new AlertDialog.Builder(this)
                    .setTitle("Exit app")
                    .setIcon(R.drawable.ichoicelogo)
                    .setMessage("Are you sure you want close session and exit the app?")
                    .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            FirebaseAuth.getInstance().signOut();
                            Log.d(TAG, "Sign out and exit");
                            finishAffinity();
                        }
                    })
                    .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    })
                    .show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void confirmUserCanChangeSettings(final String password) {
        final EditText text = new EditText(MainActivity.this);
        text.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
        alert.setTitle("Confirm access to settings")
                .setMessage("Introduce your password if you want acces to the settings")
                .setView(text)
                .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String input = text.getText().toString();
                        if (password.equals(input)){
                            startActivity(new Intent(MainActivity.this, SettingsActivity.class));
                        }else{
                            Toast.makeText(MainActivity.this, "The password is not correct", Toast.LENGTH_SHORT).show();
                        }
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .show();
    }
}