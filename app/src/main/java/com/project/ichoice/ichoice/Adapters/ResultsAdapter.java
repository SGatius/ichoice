package com.project.ichoice.ichoice.Adapters;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.project.ichoice.ichoice.Holders.MyViewHolder;
import com.project.ichoice.ichoice.Objects.ListResults;
import com.project.ichoice.ichoice.R;

import java.util.List;

public class ResultsAdapter extends RecyclerView.Adapter<MyViewHolder>{

    private List<ListResults> resultsList;

    public ResultsAdapter(List<ListResults> list){
        this.resultsList = list;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.results_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ListResults result = resultsList.get(position);
        holder.key.setText(result.getKey());
        holder.value.setText(String.valueOf(result.getValue()));
    }

    @Override
    public int getItemCount() {
        return resultsList.size();
    }
}
