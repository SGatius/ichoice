package com.project.ichoice.ichoice.Activities;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.project.ichoice.ichoice.R;

public class ChangePasswordActivity extends AppCompatActivity {

    private static final String TAG = "ChangePasswordActivity:";
    private FirebaseUser user;
    private String newPass;
    private EditText old, nw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        old = (EditText)findViewById(R.id.oldPassword);
        nw = (EditText)findViewById(R.id.newPassword);

        findViewById(R.id.btnChange).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String oldPass = old.getText().toString();
                newPass = nw.getText().toString();
                if (oldPass.isEmpty()){
                    old.setError("This field can't be empty");
                }else if (newPass.isEmpty()){
                    nw.setError("This field can't be empty");
                }else if (newPass.length() < 6){
                    nw.setError("The new password is too short.");
                }else {
                    user = FirebaseAuth.getInstance().getCurrentUser();

                    AuthCredential credential = EmailAuthProvider
                            .getCredential(user.getEmail(), oldPass);

                    user.reauthenticate(credential)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        user.updatePassword(newPass).addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (task.isSuccessful()) {
                                                    Log.d(TAG, "Password updated");
                                                    Toast.makeText(ChangePasswordActivity.this, "The password was updated", Toast.LENGTH_SHORT).show();
                                                    finish();
                                                } else {
                                                    Log.e(TAG, "Error password not updated");
                                                    Toast.makeText(ChangePasswordActivity.this, "Error to update the password", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        });
                                    } else {
                                        Log.e(TAG, "Error auth failed");
                                        Toast.makeText(ChangePasswordActivity.this, "The password isn't correct", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                }
            }
        });
    }
}
