package com.project.ichoice.ichoice.Activities;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.project.ichoice.ichoice.Builder.EBuild;
import com.project.ichoice.ichoice.Fragments.DateFragment;
import com.project.ichoice.ichoice.Fragments.DescriptionFragment;
import com.project.ichoice.ichoice.Fragments.OptionsFragment;
import com.project.ichoice.ichoice.Fragments.TitleFragment;
import com.project.ichoice.ichoice.R;

public class CreateElectionActivity extends AppCompatActivity {

    private static final String TAG = "CreateElectionActivity";
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private TitleFragment titleFragment = TitleFragment.newInstance();
    private DescriptionFragment descriptionFragment = DescriptionFragment.newInstance();
    private OptionsFragment optionsFragment = OptionsFragment.newInstance();
    private DateFragment dateFragment = DateFragment.newInstance();
    private FirebaseDatabase database;
    private DatabaseReference myRef;
    private EBuild builder = new EBuild();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_election);
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        builder.create();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkParameters(view)){
                    myRef.child("Users")
                            .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                            .child("Elections")
                            .child(builder.getElection().getId())
                            .setValue(builder.getElection());
                    for (int i = 0; i < builder.getElection().getOptions().size(); i++){
                        myRef.child("Elections Results")
                                .child(builder.getElection().getId())
                                .child(builder.getElection().getOptions().get(i))
                                .setValue(0);
                    }
                    startActivity(new Intent(view.getContext(), MainActivity.class));
                    finish();
                }
            }
        });

    }

    private boolean checkParameters(View view) {
        boolean check = true;
        if (titleFragment.isEmpty()){
            Log.e(TAG, "The title is empty");
            errorCreatingElection(view, "The title is empty");
            check = false;
        }
        else{
            builder.Title(titleFragment.getTitle());
            Log.d(TAG, "Title: " + titleFragment.getTitle());
        }
        if (descriptionFragment.isEmpty()){
            Log.e(TAG, "The description is empty");
            errorCreatingElection(view, "The description is empty");
            check = false;
        }
        else {
            builder.Description(descriptionFragment.getDescription());
            Log.d(TAG, "Description: " + descriptionFragment.getDescription());
        }
        if (!optionsFragment.checkOptions()){
            Log.e(TAG, "Some field is empty");
            errorCreatingElection(view, "Check options");
            check = false;
        }else{
            builder.Options(optionsFragment.getOptions());
            Log.d(TAG, "Options loaded on Election's object");
        }
        if (dateFragment.isAssign()){
            builder.Date(dateFragment.getDate());
            Log.d(TAG, "Data: " + dateFragment.getDate());
        }else {
            builder.Date("");
            Log.d(TAG, "Election without date");
        }
        return check;
    }

    private void errorCreatingElection(View view, String msg) {
        Snackbar.make(view, msg, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            if (position == 0){
                return titleFragment;
            }else if (position == 1){
                return descriptionFragment;
            }else if (position == 2){
                return optionsFragment;
            }else {
              return dateFragment;
            }

        }

        @Override
        public int getCount() {
            // Show 4 total pages.
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "TITLE";
                case 1:
                    return "DESCRIPTION";
                case 2:
                    return "OPTIONS";
                case 3:
                    return "DATE";
            }
            return null;
        }
    }

}
