package com.project.ichoice.ichoice.Objects;


public class ListResults {

    private String key;
    private Long value;

    public ListResults() {
    }

    public ListResults(String key, Long value){
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }
}
