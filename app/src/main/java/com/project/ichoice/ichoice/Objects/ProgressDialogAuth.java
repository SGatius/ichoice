package com.project.ichoice.ichoice.Objects;

import android.app.ProgressDialog;
import android.content.Context;


public class ProgressDialogAuth {

    private ProgressDialog progressDialog;

    public ProgressDialogAuth(Context context, String msg) {
        progressDialog = new android.app.ProgressDialog(context);
        progressDialog.setMessage(msg);
    }

    public void hideProgressDialog() {
        progressDialog.dismiss();
    }

    public void showProgressDialog() {
        progressDialog.show();
    }

    public void setMessage(String msg){
        progressDialog.setMessage(msg);
    }
}
