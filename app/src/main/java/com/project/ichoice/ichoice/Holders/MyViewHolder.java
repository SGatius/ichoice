package com.project.ichoice.ichoice.Holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.project.ichoice.ichoice.R;

public class MyViewHolder extends RecyclerView.ViewHolder {

    public TextView key, value;

    public MyViewHolder(View view){
        super(view);
        key = (TextView)view.findViewById(R.id.key);
        value = (TextView)view.findViewById(R.id.value);
    }
}
