package com.project.ichoice.ichoice.Activities;

import android.Manifest;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.SwitchPreference;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.project.ichoice.ichoice.R;


public class PreferenceSettings extends PreferenceFragment {

    private static final int REQUEST_FINGERPRINT = 7193;
    private static final String TAG = "PreferenceSettings: ";
    private SharedPreferences preferences;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        preferences.registerOnSharedPreferenceChangeListener(prefListener);
        addPreferencesFromResource(R.xml.preferences);

    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        preferences.unregisterOnSharedPreferenceChangeListener(prefListener);
    }

    SharedPreferences.OnSharedPreferenceChangeListener prefListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            Log.d(TAG, "onSharedPreferenceChanged: " + key);
            if (key.equals("user_fingerprint")){
                SwitchPreference fingerprint = (SwitchPreference)findPreference(key);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && fingerprint.isChecked()){
                    KeyguardManager keyguardManager = (KeyguardManager)getActivity()
                            .getSystemService(Context.KEYGUARD_SERVICE);
                    FingerprintManager fingerprintManager = (FingerprintManager)getActivity()
                            .getSystemService(Context.FINGERPRINT_SERVICE);
                    if (!fingerprintManager.isHardwareDetected()){
                        showToast(5, fingerprint, sharedPreferences);
                    }
                    else if (ActivityCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED){
                        ActivityCompat.requestPermissions(getActivity(),
                                new String[]{Manifest.permission.USE_FINGERPRINT},
                                REQUEST_FINGERPRINT);
                    }else if (!fingerprintManager.hasEnrolledFingerprints()){
                        showToast(3, fingerprint, sharedPreferences);
                    }
                    if (!keyguardManager.isKeyguardSecure()){
                        showToast(4, fingerprint, sharedPreferences);
                    }else {
                        showToast(1, null, null);
                    }
                }else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M){
                    showToast(6, fingerprint, sharedPreferences);
                }else{
                    showToast(2, null, null);
                }
            }
        }
    };

    private void showToast(int options, SwitchPreference fingerprint, SharedPreferences sp){
        Context c = getActivity();
        if (options == 3 || options == 4 || options == 5 || options == 6){
            fingerprint.setChecked(false);
            sp.edit().putBoolean("user_fingerprint",false).apply();
        }
        switch (options){
            case 1:
                Toast.makeText(c, R.string.fingerprint_ok, Toast.LENGTH_SHORT).show();
                break;
            case 2:
                Toast.makeText(c, R.string.fingerprint_disable, Toast.LENGTH_SHORT).show();
                break;
            case 3:
                Toast.makeText(c, R.string.fingerprint_no_config, Toast.LENGTH_SHORT).show();
                break;
            case 4:
                Toast.makeText(c, R.string.fingerprint_no_security, Toast.LENGTH_SHORT).show();
                break;
            case 5:
                Toast.makeText(c, R.string.fingerprint_no_support, Toast.LENGTH_SHORT).show();
                break;
            case 6:
                Toast.makeText(c, R.string.fingerprint_old_version, Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case REQUEST_FINGERPRINT:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    Snackbar.make(getActivity().getCurrentFocus(), "Permission granted", Snackbar.LENGTH_LONG).show();
                }else {
                    Snackbar.make(getActivity().getCurrentFocus(), "Permission denied", Snackbar.LENGTH_LONG).show();
                }
                break;
        }
    }
}