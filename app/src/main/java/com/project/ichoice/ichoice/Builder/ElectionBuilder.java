package com.project.ichoice.ichoice.Builder;

import com.project.ichoice.ichoice.Objects.Election;

import java.util.List;
import java.util.UUID;

/**
 * Created by santi on 11/06/17.
 */

public abstract class ElectionBuilder {

    protected Election election;

    public Election getElection() {
        return election;
    }

    public void create(){
        election = new Election();
        election.setId(UUID.randomUUID().toString());
    }

    public abstract void Title(String title);
    public abstract void Description(String description);
    public abstract void Options(List<String> options);
    public abstract void Date(String date);
}
