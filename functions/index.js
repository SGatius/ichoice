const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

exports.sendMessageToAdmin = functions.database.ref('/users/{userId}').onWrite(event => {


  const userId = event.params.userId;
  var eventSnapshot = event.data;
  const regToken = eventSnapshot.child("instanceId").val();
  const regEmail = eventSnapshot.child("mail").val();

   //Notification details.
    const payload = {
      notification: {
        title: 'You have a new item.',
        body: regEmail + ' share to you a new item.',
        sound: "default"
      },
    };
  return admin.messaging().sendToDevice(regToken, payload);
});
